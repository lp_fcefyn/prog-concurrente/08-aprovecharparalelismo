// Multiplicar por 10 todos los elementos de una matriz de forma secuencial y medir el tiempo que el programa demora en realizarlo
import java.util.Random;

public class Principal {
	public static void main(String[] args)
	{
		// Variables para la matriz
		int tam = 18000;
		int[][] matriz = new int[tam][tam];
		
		// Variable para generar num aleatorios
		Random rand = new Random(System.nanoTime());
		
		// Variables para medir el tiempo
		double t_inicial, t_transcurrido;
		
		// Inicializar la matriz con num enteros aleatorios entre 0 y 9 inclusive
		for (int  i = 0; i < matriz.length; i++)
		{
			for (int j = 0; j < matriz.length; j++)
			{
				matriz[i][j] = rand.nextInt(10);
			}
		}
		
		// Tiempo  inicial: Cuando empieza a multiplicar
		t_inicial = System.nanoTime();
		
		for (int  i = 0; i < matriz.length; i++)
		{
			for (int j = 0; j < matriz.length; j++)
			{
				matriz[i][j] *= 10;
			}
		}
		
		// Tiempo final cuando rermina de multiplicar
		t_transcurrido = System.nanoTime() - t_inicial;
		
		//Mostrar el resultado
		/*for (int  i = 0; i < matriz.length; i++)
		{
			for (int j = 0; j < matriz.length; j++)
			{
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println();
		}*/
		
		System.out.println("Soy el hilo principal. El resultado se obtuvo en "+t_transcurrido/1000000+" milisegundos");
	}
}
