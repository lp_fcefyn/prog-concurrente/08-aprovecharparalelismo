
public class Hilo implements Runnable{
	//Variables para determinar que porcion de la matriz resolver
	private int inicio;
	private int fin;
	
	public Hilo(){}
	
	public Hilo(int inicio, int fin)
	{
		this.inicio = inicio;
		this.fin = fin;
	}
	
	@Override
	public void run() 										
	{								
		// Este hilo resuelve el problema para las filas entre inicio y fin
		// j recorre todas las columnas de esas filas.
		for(int i = inicio; i < fin; i++)
		{
			for(int j = 0; j < Principal.matriz.length; j++)
			{
				Principal.matriz[i][j] *= 10;
			}
		}
		
	}
	
}