// Multiplicar por 10 todos los elementos la matrice de forma concurrente y medir el tiempo que el programa demora en realizarlo
import java.util.Random;

public class Principal 
{
	// Variables para la matriz; Es static para que todos los hilos puedan acceder exactamente al mismo elemento.
	private static int tam = 18000;
	public static int[][] matriz = new int[tam][tam];
		
	public static void main(String[] args)
	{
		// Variable para generar num aleatorios
		Random rand = new Random(System.nanoTime());
		
		// Vector para crear los hilos
		Thread h[] = new Thread[3];
		
		// Variables para medir el tiempo.
		double t_inicial, t_transcurrido;
		
		// Inicializar la matriz con num enteros aleatorios entre 0 y 9 inclusive
		for (int  i = 0; i < tam; i++)
		{
			for (int j = 0; j < tam; j++)
			{
				matriz[i][j] = rand.nextInt(10);
			}
		}
		
		// Se mide el tiempo desde que se lanzan los hilos para medir el tiempo para multiplicar todos los elementos de la matriz
		// + el tiempo en crear y lanzar los hilos.
		t_inicial = System.nanoTime();
		
		// Crear los hilos
		h[0] = new Thread(new Hilo(0,6000));
		h[1] = new Thread(new Hilo(6000,12000));
		h[2] = new Thread(new Hilo(12000,18000));
		
		//Lanzar los hilos
		h[0].start();
		h[1].start();
		h[2].start();
		
		// Que el hilo principal espere a los otros hilos.
		for(int i = 0;i < 3;i++)
		{
			try {
				h[i].join();
			} catch (Exception e) {}
		}
	
		// Para este punto los hilos ya terminaron de ejecutar el contenido de run()
		t_transcurrido = System.nanoTime() - t_inicial;
		
		//Mostrar el resultado
		/*for (int  i = 0; i < matriz.length; i++)
		{
			for (int j = 0; j < matriz.length; j++)
			{
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println();
		}*/
	
		System.out.println("Soy el hilo principal. El resultado se obtuvo en "+t_transcurrido/1000000+" milisegundos");
		
	}
}
